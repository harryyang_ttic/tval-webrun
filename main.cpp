#include<iostream>
#include<boost/filesystem.hpp>
#include<sqlite3.h>
#include<fstream>

using namespace std;

static int callback(void *data, int argc, char **argv, char **azColName){
   int i;
   fprintf(stderr, "%s: ", (const char*)data);
   for(i=0; i<argc; i++){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

int main(int argc, char* argv[])
{
    if(argc<4)
    {
        exit(-1);
    }
    char* path=argv[1];
    char* data_path=argv[2];
    char* email=argv[3];
    char cmd[2560], filepath[256], outpath[256];
    strcpy(filepath, path);
    strcat(filepath, "/result.zip");

    sqlite3 *db;
    int rc = sqlite3_open("app/app.sqlite", &db);
    char sql[256];
    if(!boost::filesystem::exists(filepath))
    {
        sprintf(sql,"UPDATE user set status = \"result.zip not found\" where email=\"%s\";", email);
        exit(-1);
    }
    else
    {
        sprintf(sql,"UPDATE User set status = \"Upload successful\" where email=\"%s\";",email);
    }
    char *zErrMsg = 0;
    const char* data = "Callback function called";
    rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);

    strcpy(outpath, path);
    strcat(outpath,"/disp");
    sprintf(cmd,"mkdir -p %s", outpath);
    system(cmd);

    sprintf(sql,"UPDATE User set status = \"Extracting files\" where email=\"%s\";",email);
    rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
    strcat(outpath,"/");
    sprintf(cmd,"unzip -o %s -d %s", filepath, outpath);
    system(cmd);

    sprintf(cmd,"rm %s",filepath);
    system(cmd);

    strcpy(outpath,path);
    strcat(outpath,"/seg");
    sprintf(cmd,"mkdir -p %s", outpath);
    system(cmd);

    strcpy(outpath,path);
    strcat(outpath,"/seg_planes");
    sprintf(cmd,"mkdir -p %s", outpath);
    system(cmd);

    int startId=0;
    int endId=900;

    sprintf(sql,"UPDATE User set status = \"Evaluating, may take a while.\" where email=\"%s\";",email);
    rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);

    char outSegPath[256], outSegPlanesPath[256];
    sprintf(outSegPath, "%s/seg", path);
    sprintf(outSegPlanesPath, "%s/seg_planes", path);
    for(int i=startId;i<endId;i++)
    {
        char filename[256];
        sprintf(filename, "%s/test_images/%06d.png",data_path,i);
        char outpath[256];
        sprintf(outpath, "%s/disp/%06d_left_disparity.png", path, i);

        if (boost::filesystem::exists( filename ) && boost::filesystem::exists(outpath))
        {
            sprintf(cmd,"./app/tval_slantedplaneconverter %s %s %s %s", filename, outpath, outSegPath, outSegPlanesPath);
            system(cmd);
        }
    }

    char velPath[256];
    sprintf(velPath,"%s/test_vel",data_path);
    sprintf(cmd,"./app/castRayMP -r app/baseline_all.txt -p app/proj_mat.txt -b 0.53084992269 -t 0 -u 0 -d 0 -i app/v_c_original.txt %s/ %s/ %s/ app/rot_mat_all.txt %s",velPath, outSegPath, outSegPlanesPath, path);
    cout<<cmd<<endl;
    system(cmd);

    char tpeFile[256];
    sprintf(tpeFile,"%s/tpe.txt",path);
    ifstream tpe(tpeFile);
    double tpeVal;
    tpe>>tpeVal;
    tpeVal=1-tpeVal;

    sprintf(sql,"UPDATE User set status = \"Complete. Error: %f\" where email=\"%s\";",tpeVal, email);
    rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);

    sprintf(sql,"UPDATE User set stereo_score_all = %f where email=\"%s\";",tpeVal, email);
    rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);

    sqlite3_close(db);
    return 0;
}
